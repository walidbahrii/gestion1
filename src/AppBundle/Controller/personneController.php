<?php
namespace AppBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

use AppBundle\Entity\personne;

class personneController extends Controller {



/**
* @Route("/create-personne")
*/
public function createAction(Request $request) {

  $personne = new personne();
  $form = $this->createFormBuilder($article)
    ->add('nom', TextType::class)
    ->add('prenom', TextType::class)
    ->add('email', TextareaType::class)
    ->add('save', SubmitType::class, array('label' => 'Add personne'))
    ->getForm();
    

  $form->handleRequest($request);

  if ($form->isSubmitted()) {

    $personne = $form->getData();

    $em = $this->getDoctrine()->getManager();
    $em->persist($personne);
    $em->flush();

    return $this->redirect('/view-personne/' . $personne->getId());

  }

  return $this->render(
    'personne/edit.html.twig',
    array('form' => $form->createView())
    );

}

}

?>
